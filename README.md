[LangWizard] — 多國語系檔案產生器
==================================================

Install
--------------------------------------

For read/write Excel 2010 xlsx/xlsm/xltx/xltm files:
https://pypi.org/project/openpyxl/
```bash
pip3 install openpyxl  
```  

For bundle Python application:
https://pypi.org/project/pyinstaller/
```bash
pip3 install pyinstaller  
```  


Bundle
--------------------------------------
Bundle application to .exe(dist\langWizard.exe):
```bash
npm run-script bundle
```  