import os
import json
import openpyxl

fn = input('請輸入 Excel (.xlsx) 檔案路徑: ')
wb = openpyxl.load_workbook(fn)

wb.active = 0
ws = wb.active

langs = []
langData = {}
data = {}

iRow = 0
for row in ws:
    compName = row[0].value
    iCell = 0
    for cell in row:
        if iCell > 1:
            if iRow == 0:
                langs.append(cell.value)
                langData[cell.value] = []
                #print('lang:'+cell.value)
            else:
                if not compName in data:
                    data[compName] = langData.copy()

                    for lang in data[compName]:
                        data[compName][lang] = []

                value = {}
                value[row[1].value] = cell.value
                data[compName][langs[iCell-2]].append(value)
                #print('value:'+cell.value)
        iCell=iCell+1
        
    iRow=iRow+1

print("langData...")
print(json.dumps(langData, indent=4, sort_keys=True, ensure_ascii=False))

print("read excel data...")
print(json.dumps(data, indent=4, sort_keys=True, ensure_ascii=False))

print()
print("write stringtable file...")
for comp in data:
    #print(comp)
    for lang in data[comp]:
        #print(lang)

        filePath = comp + ("" if lang == 'default' else '.' + lang) + ".xml"
        print(os.getcwd() + "\\" + filePath)
        with open(filePath, "w", encoding = 'utf8') as text_file:
            text_file.writelines("""<?xml version="1.0" encoding="utf-8"?>
<AutoWeb_Resource>
<StringTable>\n""")

            for values in data[comp][lang]:
                #print(values)
                
                for key in values:
                    #print(key)
                    text_file.writelines("  <I><C>"+key+"</C><S>"+values[key]+"</S></I>\n")

            text_file.writelines("""</StringTable>
</AutoWeb_Resource>""")

print()
input('completed...')